# https-proxy-in-openshift

This is one way to create a http(s) proxy in Kubernetes, to forward
requests from your local machine to internal services not reachable
from a public IP.

First, edit the resolver field in the script to match your cluster's
resolver.

Then login to the desired cluster with oc, and select the correct
namespace with oc project the-correct-namespace.

Then:

Terminal A:

```
$ bash create-https-proxy-in-openshift.sh
```

Terminal B:

```
$ oc port-forward pod/stupid-proxy 3128:3128
```

When that is done, you can call internal services like this:

```
$ env https_proxy=localhost:3128 curl -L -u 'basic-auth-user:basic-auth-pass' 'https://vpc-jobstream-subscriber-develop-mihlhswhyher5razq4qfqn4uni.eu-north-1.es.amazonaws.com/'
```

If you want to use your web browser, you need to configure its https proxy to `localhost:3128`.
