#!/bin/bash
set -euo pipefail
IFS=$'\n\t'


if oc get pod stupid-proxy 1>&2 2>/dev/null; then
    oc delete pod stupid-proxy
fi


{ sleep 2 ; echo "now please run: oc port-forward pod/stupid-proxy 3128:3128"; } &


oc run stupid-proxy --rm -i --image ubuntu/squid:5.2-22.04_beta --overrides='
{
        "spec": {
            "containers": [
                {
                    "image": "ubuntu/squid:5.2-22.04_beta",
                    "name": "squid",
                    "volumeMounts": [
		        {
                               "mountPath": "/run",
                               "name": "tmp"
                        },
		        {
                               "mountPath": "/var/log/squid",
                               "name": "tmp"
                        }
                    ]
                }
            ],
            "volumes": [
                {
                    "name": "tmp",
                    "emptyDir": {}
                }
            ]
        }
}
'
